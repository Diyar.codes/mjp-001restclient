<section class="mt-3">
    <div class="container">
        <table class="table table-hover text-center">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name Product</th>
                    <th scope="col">Price Product</th>
                    <th scope="col">QTY Product</th>
                    <th scope="col">Harga Total</th>
                </tr>
            </thead>
            <tbody id="contentC">

            </tbody>
        </table>
        <button type="button" class="btn btn-success btn-checkout" id="btn-checkout" name="btn-checkout">checkout</button>
    </div>
</section>