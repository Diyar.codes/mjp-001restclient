<section class="mt-3">
    <div class="container">
        <div class="row" id="contentH">

        </div>
    </div>
</section>

<div class="modal fade" id="basicExampleModalGift" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <section id="formGift" class="formGift" name="formGift">
                    <div class="card">
                        <div class="card-body">
                            <form action="" id="GiftForm">
                                <input type="hidden" name="id_user" id="id_user" class="is_user">
                                <input type="hidden" name="id_hadiah" id="id_hadiah" class="id_hadiah">
                                <input type="hidden" name="nama_hadiah" id="nama_hadiah" class="nama_hadiah">
                                <input type="hidden" name="point" id="point" class="point">
                                <div class="form-group">

                                </div>
                                <button type="submit" class="btn btn-sm btn-primary btn-gift" id="btn-gift" name="btn-gift">Save</button>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>