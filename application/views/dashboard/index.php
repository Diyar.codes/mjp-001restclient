<section class="mt-3">
    <div class="container">
        <div class="row" id="contentP">

        </div>
    </div>
</section>

<div class="modal fade" id="basicExampleModalCard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <section id="formQTY" class="formQTY" name="formQTY">
                    <div class="card">
                        <div class="card-body">
                            <form action="" id="qtyForm">
                                <input type="hidden" name="id_user" id="id_user" class="is_user">
                                <input type="hidden" name="id_product" id="id_product" class="id_product">
                                <input type="hidden" name="name_product" id="name_product" class="name_product">
                                <input type="hidden" name="price_product" id="price_product" class="price_product">
                                <div class="form-group">
                                    <input type="text" class="form-control qty_product" name="qty_product" id="qty_product" autocomplete="off" placeholder="Input Qty">
                                </div>
                                <button type="submit" class="btn btn-sm btn-primary btn-cart" id="btn-cart" name="btn-cart">Save</button>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>