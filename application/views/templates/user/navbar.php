<nav class="navbar navbar-dark primary-color">
    <div class="container">
        <a class="navbar-brand" href="#">
            <img src="https://mdbootstrap.com/img/logo/mdb-transparent.png" height="30" alt="mdb logo">
        </a>
        <a href="<?= base_url('Auth/logout'); ?>" class="btn btn-danger btn-sm">Logout</a>
    </div>
</nav>

<div class="row">
    <div class="col-md-1 mt-1 ml-4">
        <a href="<?= base_url('Dashboard'); ?>"><button type="button" class="btn btn-primary btn-sm btn-rounded"><i class="fas fa-heart pr-2" aria-hidden="true"></i>Product</button></a>
    </div>
    <div class="col-md-1 mt-1 ml-4">
        <a href="<?= base_url('Dashboard/Gift'); ?>"><button type="button" class="btn btn-pink btn-sm btn-rounded"><i class="far fa-thumbs-up" aria-hidden="true"></i>Ghift</button></a>
    </div>
    <div class="col-md-1 mt-1 ml-4">
        <a href="<?= base_url('Dashboard/checkout'); ?>"><button type="button" class="btn btn-success btn-sm btn-rounded"><i class="fas fa-balance-scale" aria-hidden="true"></i>Chekcout</button></a>
    </div>
    <div class="col-md-1 mt-1 ml-4">
        <a href="<?= base_url('Dashboard/point'); ?>"><button type="button" class="btn btn-danger btn-sm btn-rounded"><i class="fas fa-star"></i>Point</button></a>
    </div>
</div>