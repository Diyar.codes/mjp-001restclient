<nav class="navbar navbar-dark primary-color">
    <div class="container">
        <a class="navbar-brand" href="#">
            <img src="https://mdbootstrap.com/img/logo/mdb-transparent.png" height="30" alt="mdb logo">
        </a>
        <a href="<?= base_url('Auth/logout'); ?>" class="btn btn-danger btn-sm">Logout</a>
    </div>
</nav>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModalInsert">
    Tambah Data
</button>

<!-- Modal Insert -->
<div class="modal fade" id="basicExampleModalInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">E-Laptopgift</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <section id="formTambahProduk" class="formTambahProduk" name="formTambahProduk">
                    <div class="card">
                        <div class="card-body">
                            <form action="" id="insertForm">
                                <div class="text-center">
                                    <h1 class="card-title">Form Tambah Hadiah</h1>
                                </div>
                                <div class="form-group">
                                    <label for="name_gift">Name Gift</label>
                                    <input type="text" class="form-control name_gift" name="name_gift" id="name_gift" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label for="point_gift">Point Gift</label>
                                    <input type="text" class="form-control point_gift" name="point_gift" id="point_gift" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label for="image">Gambar</label>
                                    <input type="file" class="form-control image" name="image" id="image">
                                </div>
                                <button type="submit" class="btn btn-sm btn-primary btn-insert" id="btn-insert" name="btn-insert">Save</button>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="basicExampleModalUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">E-LaptopMerce</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <section id="formEditHadiah" class="formEditHadiah" name="formEditHadiah">
                    <div class="card">
                        <div class="card-body">
                            <form action="" id="updateForm">
                                <div class="text-center">
                                    <h1 class="card-title">Form Ubah Hadiah</h1>
                                </div>
                                <input type="hidden" name="id_gift" id="id_gift" class="id_gift">
                                <div class="form-group">
                                    <label for="name_gift">Name Gift</label>
                                    <input type="text" class="form-control name_gift" name="name_gift" id="name_gift" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label for="point_gift">Point Gift</label>
                                    <input type="text" class="form-control point_gift" name="point_gift" id="point_gift" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label for="image">Gambar</label>
                                    <input type="file" class="form-control image" name="image" id="image">
                                </div>
                                <button type="submit" class="btn btn-sm btn-primary btn-update" id="btn-update" name="btn-update">Save</button>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<section class="mt-3">
    <div class="container">
        <table class="table table-hover text-center">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name Gift</th>
                    <th scope="col">Point Gift</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody id="contentP">

            </tbody>
        </table>
    </div>
</section>