<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    // public function __construct()
    // {
    //     parent::__construct();
    //     if ($this->session->userdata() == true) {
    //         redirect(base_url('Auth/block'));
    //     }
    // }

    function index()
    {
        $data['title'] = 'Login';

        $this->load->view('templates/auth/header', $data);
        $this->load->view('auth/login');
        $this->load->view('templates/auth/footer');
    }

    function register()
    {
        $data['title'] = 'Register';

        $this->load->view('templates/auth/header', $data);
        $this->load->view('auth/register');
        $this->load->view('templates/auth/footer');
    }

    function confirmLogin($token, $id_user, $level)
    {
        if ($token == "" || $id_user == "" || $level == "") {
            redirect(base_url('auth'));
        } else {
            if ($level == "admin") {
                $this->session->set_userdata([
                    'token' => $token,
                    'id_user' => $id_user,
                    'level' => $level
                ]);
                redirect(base_url('Home'));
            } else {
                $this->session->set_userdata([
                    'token' => $token,
                    'id_user' => $id_user,
                    'level' => $level
                ]);
                redirect(base_url('Dashboard'));
            }
        }
    }

    function logout()
    {
        $this->session->unset_userdata('token');
        $this->session->unset_userdata('id_user');
        $this->session->unset_userdata('level');

        redirect(base_url('Auth'));
    }

    function block()
    {
        echo "404 Not Found";
    }
}
