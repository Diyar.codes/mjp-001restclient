<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('level') != 'user') {
            redirect(base_url('Auth/block'));
        }
    }

    function index()
    {
        $data['title'] = 'E-Laptopmerce';

        $this->load->view('templates/user/header', $data);
        $this->load->view('templates/user/navbar');
        $this->load->view('dashboard/index');
        $this->load->view('templates/user/footerP');
    }

    function gift()
    {
        $data['title'] = 'E-Laptopgift';

        $this->load->view('templates/user/header', $data);
        $this->load->view('templates/user/navbar');
        $this->load->view('dashboard/gift');
        $this->load->view('templates/user/footerH');
    }

    function checkout()
    {
        $data['title'] = 'E-Laptopgift';

        $this->load->view('templates/user/header', $data);
        $this->load->view('templates/user/navbar');
        $this->load->view('dashboard/checkout');
        $this->load->view('templates/user/footerC');
    }

    function point()
    {
        $data['title'] = 'E-Laptopgift';

        $this->load->view('templates/user/header', $data);
        $this->load->view('templates/user/navbar');
        $this->load->view('dashboard/point');
        $this->load->view('templates/user/footerPoint');
    }
}
