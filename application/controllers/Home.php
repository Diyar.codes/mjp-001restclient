<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('level') != 'admin') {
            redirect(base_url('Auth/block'));
        }
    }

    function index()
    {
        $data['title'] = 'Home Admin';

        $this->load->view('templates/admin/header', $data);
        $this->load->view('home/index');
        $this->load->view('templates/admin/footerP');
    }

    function gift()
    {
        $data['title'] = 'Gift Admin';

        $this->load->view('templates/admin/header', $data);
        $this->load->view('home/gift');
        $this->load->view('templates/admin/footerH');
    }
}
