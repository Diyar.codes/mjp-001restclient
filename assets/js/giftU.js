$(document).ready(function () {
	var token = $("body").data("token");
	var idUser = $("body").data("id_user");

	getDataGift();

	function getDataGift() {
		var form = new FormData();
		var settings = {
			"url": "http://localhost/mjp-001RestServer/gift",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonData = JSON.parse(response);
			let jsonDataG = jsonData.data;
			$(".remove").remove();
			$.each(jsonDataG, function (i, data) {
				$('#contentH').append(`
                    <div class="col-lg-3 mb-4 remove">
                        <div class="card h-100">
                            <div class="view overlay">
                                <img class="card-img-top" src="http://localhost/mjp-001RestServer/assets/img/${data.image}" alt="Card image cap" height="125">
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">${data.name_gift}</h4>
								<p class="card-text">${data.point_gift} Point</p>
								<button type="button" class="btn btn-primary addToGift" id="addToGift" name="addToGift" data-toggle="modal" data-target="#basicExampleModalGift" data-id=` + data.id_gift + `>
								exchange gifts</button>
                            </div>
                        </div>
                    </div>
                `);
			});
		});
	};

	$(document).on('click', '#addToGift', function () {
		var form = new FormData();
		form.append("id_gift", $(this).attr('data-id'));

		var settings = {
			"url": "http://localhost/mjp-001RestServer/Mygift?id_gift=" + $(this).attr('data-id'),
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": token,
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			var gift = JSON.parse(response);
			var dataG = gift.data;
			$('#GiftForm input[name = "id_user"]').val(idUser);
			$('#GiftForm input[name = "id_hadiah"]').val(dataG.id_gift);
			$('#GiftForm input[name = "nama_hadiah"]').val(dataG.name_gift);
			$('#GiftForm input[name = "point"]').val(dataG.point_gift);
		});
	});

	$('#btn-gift').click(function (e) {
		e.preventDefault();
		var form = new FormData();
		form.append("id_user", $('#GiftForm input[name = "id_user"]').val());
		form.append("id_gift", $('#GiftForm input[name = "id_hadiah"]').val());
		form.append("name_gift", $('#GiftForm input[name = "nama_hadiah"]').val());
		form.append("point_gift", $('#GiftForm input[name = "point"]').val());

		var settings = {
			"url": "http://localhost/mjp-001RestServer/Mygift",
			"method": "POST",
			"timeout": 0,
			"headers": {
				"X-Token": token,
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form,
			error: function (jqXHR, textStatus, errorThrown) {
				var err = JSON.parse(jqXHR.responseText)
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: [err.message]
				})
			}
		};

		$.ajax(settings).done(function (response) {
			console.log(response.pointSisa)
			Swal.fire({
				position: 'center',
				icon: 'success',
				title: 'Hadiah Dapat Ditukarkan',
				showConfirmButton: false,
				timer: 2000
			})
			$("#GiftForm")[0].reset()
			$('#basicExampleModalGift').modal('hide')
			getDataGift()
		});
	})
})
