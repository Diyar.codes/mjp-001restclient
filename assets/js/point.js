$(document).ready(function () {
	var token = $("body").data("token");
	var idUser = $("body").data("id_user");

	getDataPoint();

	function getDataPoint() {
		var form = new FormData();
		form.append("id_user", idUser);

		var settings = {
			"url": "http://localhost/mjp-001RestServer/point",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonData = JSON.parse(response);
			let jsonDataPoint = jsonData.data;
			$(".remove").remove();
			$('#contentPoint').append(`
                <div class="col-lg-3 mb-4 remove">
                    <div class="card h-100">
                        <div class="card-body">
                            <h4 class="card-title">${jsonDataPoint.point} Point</h4>
                        </div>
                    </div>
                </div>
            `);
		});
	};
})
