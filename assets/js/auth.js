$(document).ready(function () {
	$('#btnRegister').click(function (e) {
		e.preventDefault()
		var form = new FormData();
		form.append("username", $('#formRegister input[name = "username"]').val());
		form.append("email", $('#formRegister input[name = "email"]').val());
		form.append("password", $('#formRegister input[name = "password"]').val());
		form.append("passwordConfirm", $('#formRegister input[name = "passwordConfirm"]').val());

		var settings = {
			"url": "http://localhost/mjp-001RestServer/auth/register",
			"method": "POST",
			"timeout": 0,
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form,
			error: function (jqXHR, textStatus, errorThrown) {
				var err = JSON.parse(jqXHR.responseText)
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: [err.message]
				})
			}
		};

		$.ajax(settings).done(function (response) {
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000,
				timerProgressBar: true,
				onOpen: (toast) => {
					toast.addEventListener('mouseenter', Swal.stopTimer)
					toast.addEventListener('mouseleave', Swal.resumeTimer)
				}
			})

			Toast.fire({
				icon: 'success',
				title: 'Register in successfully'
			});

			setTimeout(function () {
				window.location.href = "http://localhost/mjp-001RestClient/auth/";
			}, 3000);
		});
	})

	$('#btnLogin').click(function (e) {
		e.preventDefault()
		var form = new FormData();
		form.append("email", $('#formLogin input[name = "email"]').val());
		form.append("password", $('#formLogin input[name = "password"]').val());

		var settings = {
			"url": "http://localhost/mjp-001RestServer/auth/login",
			"method": "POST",
			"timeout": 0,
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form,
			error: function (jqXHR, textStatus, errorThrown) {
				var err = JSON.parse(jqXHR.responseText)
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: [err.message]
				})
			}
		};

		$.ajax(settings).done(function (response) {
			var dataLogin = JSON.parse(response);
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000,
				timerProgressBar: true,
				onOpen: (toast) => {
					toast.addEventListener('mouseenter', Swal.stopTimer)
					toast.addEventListener('mouseleave', Swal.resumeTimer)
				}
			})

			Toast.fire({
				icon: 'success',
				title: 'Login in successfully'
			});

			setTimeout(function () {
				window.location.href = "http://localhost/mjp-001RestClient/auth/confirmLogin/" +
					dataLogin.token + "/" +
					dataLogin.data.id_user + "/" +
					dataLogin.data.level;
			}, 3000);
		});
	})
})
