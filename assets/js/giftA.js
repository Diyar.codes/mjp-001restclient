$(document).ready(function () {
	var token = $("body").data("token");

	getDataGift();

	function getDataGift() {
		var form = new FormData();
		var settings = {
			"url": "http://localhost/mjp-001RestServer/gift",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonData = JSON.parse(response);
			let jsonDataG = jsonData.data;
			$(".remove").remove();
			$.each(jsonDataG, function (i, data) {
				$('#contentP').append(`
                    <tr class="remove">
                        <th scope="row">${i+1}</th>
                        <td>${data.name_gift}</td>
                        <td>${data.point_gift} Point</td>
                        <td>
                        	<button type="button" class="btn btn-primary btn-sm btn-ubah" id="btn-ubah" name="btn-ubah" data-toggle="modal" data-target="#basicExampleModalUpdate" data-id=` + data.id_gift + `>Ubah</button>
                        	<a href="" class="btn btn-danger btn-sm btn-hapus" data-id=` + data.id_gift + ` id="btn-hapus" name="btn-hapus">Hapus</a>  
                        </td>
                    </tr>
                `);
			});
		});
	};

	$('#btn-insert').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'Yakin ingin menambahkan barang ini?',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Upload Data!'
		}).then((result) => {
			if (result.value) {
				var file = $('#image')[0].files[0]
				var form = new FormData();
				form.append("name_gift", $('#formTambahProduk input[name = "name_gift"]').val());
				form.append("point_gift", $('#formTambahProduk input[name = "point_gift"]').val());
				form.append("image", file);

				var settings = {
					"url": "http://localhost/mjp-001RestServer/gift",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": token,
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Data berhasil diupload',
						showConfirmButton: false,
						timer: 1000
					})
					$("#insertForm")[0].reset()
					$('#basicExampleModalInsert').modal('hide')
					getDataGift();
				});
			}
		})
	})

	$(document).on('click', '#btn-hapus', function (e) {
		e.preventDefault()
		Swal.fire({
			title: 'Apakah Anda Ingin Menghapus Data?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya'
		}).then((result) => {
			console.log(result);
			if (result.value) {
				var settings = {
					"url": "http://localhost/mjp-001RestServer/gift",
					"method": "DELETE",
					"timeout": 0,
					"headers": {
						"X-Token": token,
					},
					"data": {
						"id_gift": $(this).attr('data-id')
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire(
						'Good job!',
						'You clicked the Deleted!',
						'success'
					)
					getDataGift()
				});
			}
		})
	})

	$(document).on('click', '#btn-ubah', function () {
		var form = new FormData();
		form.append("id_gift", $(this).attr('data-id'));

		var settings = {
			"url": "http://localhost/mjp-001RestServer/gift?id_gift=" + $(this).attr('data-id'),
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": token,
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			var produk = JSON.parse(response);
			var dataH = produk.data;
			$('#formEditHadiah input[name = "name_gift"]').val(dataH.name_gift);
			$('#formEditHadiah input[name = "point_gift"]').val(dataH.point_gift);
			$('#formEditHadiah input[name = "id_gift"]').val(dataH.id_gift);
		});
	});

	$('#btn-update').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'Yakin ingin mengubah barang ini?',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Upload Data!'
		}).then((result) => {
			if (result.value) {
				var form = new FormData();
				var file = $('#formEditHadiah input[name = "image"]')[0].files[0];
				form.append("name_gift", $('#formEditHadiah input[name = "name_gift"]').val());
				form.append("point_gift", $('#formEditHadiah input[name = "point_gift"]').val());
				form.append("image", file);
				form.append("id_gift", $('#formEditHadiah input[name = "id_gift"]').val());
				form.append("_method", "PUT");

				var settings = {
					"url": "http://localhost/mjp-001RestServer/gift",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": token,
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Data berhasil diubah',
						showConfirmButton: false,
						timer: 1000
					})
					$("#updateForm")[0].reset()
					$('#basicExampleModalUpdate').modal('hide')
					getDataGift();
				});
			}
		})
	})
})
