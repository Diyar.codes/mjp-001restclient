$(document).ready(function () {
	var token = $("body").data("token")
	var idUser = $("body").data("id_user")

	getDataCheckout();

	function getDataCheckout() {
		var form = new FormData();
		var settings = {
			"url": "http://localhost/mjp-001RestServer/TransaksiProduk",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonData = JSON.parse(response);
			let jsonDataC = jsonData.data;
			$(".remove").remove();
			$.each(jsonDataC, function (i, data) {
				$('#contentC').append(`
					<tr class="remove">
                        <th scope="row">${i+1}</th>
                        <td>${data.name_product}</td>
                        <td>${data.price_product}</td>
                        <td>${data.qty_product}</td>
                        <td>${data.harga_total}</td>
					</tr>
                `);
			});
		});
	};

	$('#btn-checkout').click(function (e) {
		Swal.fire({
			title: 'Yakin ingin melakukan checkout?',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Checkout Data!'
		}).then((result) => {
			if (result.value) {
				e.preventDefault();
				var form = new FormData();
				form.append("id_user", idUser);

				var settings = {
					"url": "http://localhost/mjp-001RestServer/TransaksiProduk/checkout",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": token,
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
					}
				};

				$.ajax(settings).done(function (response) {
					var dataC = JSON.parse(response)
					console.log(dataC.message);
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: dataC.message,
						text: dataC.point,
						showConfirmButton: false,
						timer: 5000
					})
					getDataCheckout();
				});
			}
		})
	})
})
