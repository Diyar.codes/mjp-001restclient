$(document).ready(function () {
	var token = $("body").data("token");

	getDataProduct();

	function getDataProduct() {
		var form = new FormData();
		var settings = {
			"url": "http://localhost/mjp-001RestServer/Dashboard",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonData = JSON.parse(response);
			let jsonDataP = jsonData.data;
			$(".remove").remove();
			$.each(jsonDataP, function (i, data) {
				$('#contentP').append(`
                    <tr class="remove">
                        <th scope="row">${i+1}</th>
                        <td>${data.name_product}</td>
                        <td>${data.price_product}</td>
                        <td>${data.description_product}</td>
                        <td>
                        	<button type="button" class="btn btn-primary btn-sm btn-ubah" id="btn-ubah" name="btn-ubah" data-toggle="modal" data-target="#basicExampleModalUpdate" data-id=` + data.id_product + `>Ubah</button>
                        	<a href="" class="btn btn-danger btn-sm btn-hapus" data-id=` + data.id_product + ` id="btn-hapus" name="btn-hapus">Hapus</a>  
                        </td>
                    </tr>
                `);
			});
		});
	};

	$('#btn-insert').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'Yakin ingin menambahkan barang ini?',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Upload Data!'
		}).then((result) => {
			if (result.value) {
				var file = $('#image')[0].files[0]
				var form = new FormData();
				form.append("name_product", $('#formTambahProduk input[name = "name_product"]').val());
				form.append("price_product", $('#formTambahProduk input[name = "price_product"]').val());
				form.append("description_product", $('#formTambahProduk input[name = "description_product"]').val());
				form.append("image", file);

				var settings = {
					"url": "http://localhost/mjp-001RestServer/Dashboard",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": token,
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Data berhasil diupload',
						showConfirmButton: false,
						timer: 1000
					})
					$("#insertForm")[0].reset()
					$('#basicExampleModalInsert').modal('hide')
					getDataProduct();
				});
			}
		})
	})

	$(document).on('click', '#btn-hapus', function (e) {
		e.preventDefault()
		Swal.fire({
			title: 'Apakah Anda Ingin Menghapus Data?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya'
		}).then((result) => {
			console.log(result);
			if (result.value) {
				var settings = {
					"url": "http://localhost/mjp-001RestServer/Dashboard",
					"method": "DELETE",
					"timeout": 0,
					"headers": {
						"X-Token": token,
					},
					"data": {
						"id_product": $(this).attr('data-id')
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire(
						'Good job!',
						'You clicked the Deleted!',
						'success'
					)
					getDataProduct()
				});
			}
		})
	})

	$(document).on('click', '#btn-ubah', function () {
		var form = new FormData();
		form.append("id_product", $(this).attr('data-id'));

		var settings = {
			"url": "http://localhost/mjp-001RestServer/Dashboard?id_product=" + $(this).attr('data-id'),
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": token,
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			var produk = JSON.parse(response);
			var dataP = produk.data;
			$('#formEditProduk input[name = "name_product"]').val(dataP.name_product);
			$('#formEditProduk input[name = "price_product"]').val(dataP.price_product);
			$('#formEditProduk input[name = "description_product"]').val(dataP.description_product);
			$('#formEditProduk input[name = "id_product"]').val(dataP.id_product);
		});
	});

	$('#btn-update').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'Yakin ingin mengubah barang ini?',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Upload Data!'
		}).then((result) => {
			if (result.value) {
				var form = new FormData();
				var file = $('#formEditProduk input[name = "image"]')[0].files[0];
				form.append("name_product", $('#formEditProduk input[name = "name_product"]').val());
				form.append("price_product", $('#formEditProduk input[name = "price_product"]').val());
				form.append("description_product", $('#formEditProduk input[name = "description_product"]').val());
				form.append("image", file);
				form.append("id_product", $('#formEditProduk input[name = "id_product"]').val());
				form.append("_method", "PUT");

				var settings = {
					"url": "http://localhost/mjp-001RestServer/Dashboard",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": token,
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Data berhasil diubah',
						showConfirmButton: false,
						timer: 1000
					})
					$("#updateForm")[0].reset()
					$('#basicExampleModalUpdate').modal('hide')
					getDataProduct();
				});
			}
		})
	})
})
