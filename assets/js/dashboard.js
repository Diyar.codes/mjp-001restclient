$(document).ready(function () {
	var token = $("body").data("token");
	var idUser = $("body").data("id_user");

	getDataProduct();

	function getDataProduct() {
		var form = new FormData();
		var settings = {
			"url": "http://localhost/mjp-001RestServer/Dashboard",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonData = JSON.parse(response);
			let jsonDataP = jsonData.data;
			$(".remove").remove();
			$.each(jsonDataP, function (i, data) {
				$('#contentP').append(`
					<div class="col-lg-3 mb-4 remove">
                        <div class="card h-100" data-target="#basicExampleModalCard">
                            <div class="view overlay">
                                <img class="card-img-top" src="http://localhost/mjp-001RestServer/assets/img/${data.image}" alt="Card image cap" height="125">
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">${data.name_product}</h4>
                                <p class="card-text"> Rp. ${data.price_product}</p>
							</div>
							<div class="card-footer">
								<button type="button" class="btn btn-primary addToCart" id="addToCart" name="addToCart" data-toggle="modal" data-target="#basicExampleModalCard" data-id=` + data.id_product + `>Add to Cart</button>
							</div>
                        </div>
                    </div>
                `);
			});
		});
	};

	$(document).on('click', '#addToCart', function () {
		var form = new FormData();
		form.append("id_product", $(this).attr('data-id'));

		var settings = {
			"url": "http://localhost/mjp-001RestServer/Dashboard?id_product=" + $(this).attr('data-id'),
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": token,
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			var produk = JSON.parse(response);
			var dataP = produk.data;
			$('#qtyForm input[name = "id_user"]').val(idUser);
			$('#qtyForm input[name = "id_product"]').val(dataP.id_product);
			$('#qtyForm input[name = "name_product"]').val(dataP.name_product);
			$('#qtyForm input[name = "price_product"]').val(dataP.price_product);
			$('#qtyForm input[name = "qty_product"]').val();
		});
	});

	$('#btn-cart').click(function (e) {
		e.preventDefault();
		var form = new FormData();
		form.append("id_user", $('#formQTY input[name = "id_user"]').val());
		form.append("id_product", $('#formQTY input[name = "id_product"]').val());
		form.append("name_product", $('#formQTY input[name = "name_product"]').val());
		form.append("price_product", $('#formQTY input[name = "price_product"]').val());
		form.append("qty_product", $('#formQTY input[name = "qty_product"]').val());

		var settings = {
			"url": "http://localhost/mjp-001RestServer/TransaksiProduk",
			"method": "POST",
			"timeout": 0,
			"headers": {
				"X-Token": token,
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form,
			error: function (jqXHR, textStatus, errorThrown) {
				var err = JSON.parse(jqXHR.responseText)
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: [err.message]
				})
			}
		};

		$.ajax(settings).done(function (response) {
			Swal.fire({
				position: 'center',
				icon: 'success',
				title: 'Silahkan ke Halaman checkout',
				showConfirmButton: false,
				timer: 2000
			})
			$("#qtyForm")[0].reset()
			$('#basicExampleModalCard').modal('hide')
			getDataProduct();
		});
	})
})
